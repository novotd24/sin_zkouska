package com.example.zkouska.service;

import com.example.zkouska.model.Belief;
import com.example.zkouska.model.Group;
import com.example.zkouska.repository.GroupRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

public class GroupServiceImplTest {

    @Mock
    private GroupRepository groupRepository;

    @InjectMocks
    private GroupServiceImpl groupService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testFightGroupsExistAndDifferentBeliefs() throws Exception {
        Group group1 = new Group(10, 20, 10, Belief.GOOD, new ArrayList<>());
        Group group2 = new Group(15, 10, 12, Belief.EVIL, new ArrayList<>());
        when(groupRepository.findById(1L)).thenReturn(Optional.of(group1));
        when(groupRepository.findById(2L)).thenReturn(Optional.of(group2));

        Group result = groupService.fight(1L, 2L);

        assertNotNull(result);
        assertNotEquals(group1.getBelief(), group2.getBelief());
    }

    @Test
    void testFightGroupsNotExist() {
        when(groupRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(Exception.class, () -> groupService.fight(1L, 2L));
    }

    @Test
    void testFightGroupsExistSameBeliefs() {
        Group group1 = new Group(10, 20, 10, Belief.GOOD, new ArrayList<>());
        when(groupRepository.findById(1L)).thenReturn(Optional.of(group1));
        when(groupRepository.findById(2L)).thenReturn(Optional.of(group1));
        assertThrows(Exception.class, () -> groupService.fight(1L, 2L));
    }
}
