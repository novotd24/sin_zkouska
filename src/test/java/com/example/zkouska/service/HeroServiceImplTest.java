package com.example.zkouska.service;

import com.example.zkouska.model.Belief;
import com.example.zkouska.model.Group;
import com.example.zkouska.model.Hero;
import com.example.zkouska.repository.GroupRepository;
import com.example.zkouska.repository.HeroRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class HeroServiceImplTest {

    @Mock
    private HeroRepository heroRepository;

    @Mock
    private GroupRepository groupRepository;

    @InjectMocks
    private HeroServiceImpl heroService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testSaveHeroUniqueName() {

        Group goodGroup = new Group(0,0,0, Belief.GOOD, new ArrayList<>());
        Hero strongBilbo = new Hero("bilbo", Belief.GOOD, 10,10,10, goodGroup);
        Hero weakBiblo = new Hero("bilbo", Belief.GOOD, 0,0,0, null);
        heroRepository.save(strongBilbo);

        heroService.saveHero("bilbo", Belief.GOOD, 10, 10, 10);
        verify(heroRepository, times(0)).save(weakBiblo);
    }

    @Test
    public void testAddTwoHeroesToGroup() {
        Group goodGroup = new Group(0, 0, 0, Belief.GOOD, new ArrayList<>());
        goodGroup.setId(3L);
        groupRepository.save(goodGroup);
        Hero thorin = new Hero("thorin", Belief.GOOD, 10, 5, 0, goodGroup);
        thorin.setId(1L);
        Hero strongBilbo = new Hero("bilbo", Belief.GOOD, 10,10,10, goodGroup);
        strongBilbo.setId(2L);

        when(heroRepository.findById(1L)).thenReturn(Optional.of(thorin));
        when(heroRepository.findById(2L)).thenReturn(Optional.of(strongBilbo));
        when(groupRepository.findById(3L)).thenReturn(Optional.of(goodGroup));

        heroService.addHeroToGroup(thorin.getId(), goodGroup.getId());
        heroService.addHeroToGroup(strongBilbo.getId(), goodGroup.getId());

        assertTrue(goodGroup.getHeroes().contains(thorin));
        assertTrue(goodGroup.getHeroes().contains(strongBilbo));
    }

    @Test
    public void testAddHeroToGroupWithDifferentBeliefs() {
        Group goodGroup = new Group(0, 0, 0, Belief.GOOD, new ArrayList<>());
        goodGroup.setId(3L);
        groupRepository.save(goodGroup);
        Hero thorin = new Hero("thorin", Belief.GOOD, 10, 5, 0, goodGroup);
        thorin.setId(1L);
        Hero bolg = new Hero("bolg", Belief.EVIL, 10, 10, 0, null);
        bolg.setId(2L);

        when(heroRepository.findById(1L)).thenReturn(Optional.of(thorin));
        when(heroRepository.findById(2L)).thenReturn(Optional.of(bolg));
        when(groupRepository.findById(3L)).thenReturn(Optional.of(goodGroup));

        heroService.addHeroToGroup(bolg.getId(), goodGroup.getId());

        assertFalse(goodGroup.getHeroes().contains(bolg));
    }

    @Test
    public void testAddHeroToGroupWithMatchingBeliefs() {
        Group goodGroup = new Group(0, 0, 0, Belief.GOOD, new ArrayList<>());
        goodGroup.setId(3L);
        groupRepository.save(goodGroup);
        Hero thorin = new Hero("thorin", Belief.GOOD, 10, 5, 0, goodGroup);
        thorin.setId(1L);
        Hero aragorn = new Hero("aragorn", Belief.GOOD, 15, 8, 5, null);
        aragorn.setId(2L);
        when(heroRepository.findById(1L)).thenReturn(Optional.of(thorin));
        when(heroRepository.findById(2L)).thenReturn(Optional.of(aragorn));
        when(groupRepository.findById(3L)).thenReturn(Optional.of(goodGroup));

        heroService.addHeroToGroup(aragorn.getId(), goodGroup.getId());
        assertTrue(goodGroup.getHeroes().contains(aragorn));
        assertEquals(goodGroup, aragorn.getGroup());
    }
}

