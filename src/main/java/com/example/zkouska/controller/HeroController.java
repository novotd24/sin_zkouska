package com.example.zkouska.controller;

import com.example.zkouska.model.Belief;
import com.example.zkouska.model.Hero;
import com.example.zkouska.service.HeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HeroController {

    private HeroService heroService;

    @Autowired
    public HeroController(HeroService heroService) {
        this.heroService = heroService;
    }

    //vse funguje az na to ze tady se vraci null
    @GetMapping("/add/hero/{heroId}/group/{groupId}")
    public ResponseEntity<Hero> addHero(@PathVariable Long heroId, @PathVariable Long groupId) {
        Hero hero = heroService.addHeroToGroup(heroId, groupId);
        return ResponseEntity.ok().body(hero);
    }

    @GetMapping("/get/{heroId}")
    public Hero getHero(@PathVariable Long heroId) throws Exception {
        return heroService.getById(heroId);
    }

    @GetMapping("/getAll")
    public List<Hero> getAll() throws Exception {
        return heroService.getAll();
    }

    @PostMapping("/saveHero")
    public void saveHero(@RequestParam String name,
                         @RequestParam Belief belief,
                         @RequestParam int strength,
                         @RequestParam int will,
                         @RequestParam int magic) {
        heroService.saveHero(name, belief, strength, will, magic);
    }

    @PostMapping("/updateHero/hero/{heroId}")
    public void updateHero(@PathVariable Long heroId,
                         @RequestParam String name,
                         @RequestParam Belief belief,
                         @RequestParam int strength,
                         @RequestParam int will,
                         @RequestParam int magic) {
        heroService.updateHero(heroId ,name, belief, strength, will, magic);
    }
}
