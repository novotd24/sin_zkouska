package com.example.zkouska.controller;

import com.example.zkouska.model.Group;
import com.example.zkouska.model.Hero;
import com.example.zkouska.repository.GroupRepository;
import com.example.zkouska.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class GroupController {

    private GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping("/fight/group1/{group1Id}/group2/{group2Id}")
    public ResponseEntity<Group> fight(@PathVariable Long group1Id, @PathVariable Long group2Id) throws Exception {
        Group winner = groupService.fight(group1Id, group2Id);
        return ResponseEntity.ok().body(winner);
    }
}
