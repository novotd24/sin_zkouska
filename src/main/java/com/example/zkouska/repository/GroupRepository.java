package com.example.zkouska.repository;

import com.example.zkouska.model.Group;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepository extends CrudRepository<Group, Long> {
}
