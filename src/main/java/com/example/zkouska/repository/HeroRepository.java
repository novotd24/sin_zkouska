package com.example.zkouska.repository;

import com.example.zkouska.model.Hero;
import org.springframework.data.repository.CrudRepository;

public interface HeroRepository extends CrudRepository<Hero, Long> {

}
