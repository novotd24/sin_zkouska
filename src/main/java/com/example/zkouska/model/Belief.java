package com.example.zkouska.model;

public enum Belief {
    GOOD, EVIL, NONE
}
