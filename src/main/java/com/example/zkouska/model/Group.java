package com.example.zkouska.model;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Group extends AbstractEntity {

    private int totalStrength;
    private int totalWill;
    private int totalMagic;
    private Belief belief;

    @OneToMany(mappedBy = "group")
    private List<Hero> heroes;

    public Group() {
    }

    public Group(int totalStrength, int totalWill, int totalMagic, Belief belief, List<Hero> heroes) {
        this.totalStrength = totalStrength;
        this.totalWill = totalWill;
        this.totalMagic = totalMagic;
        this.belief = belief;
        this.heroes = heroes;
    }

    public int getTotalStrength() {
        return totalStrength;
    }

    public void setTotalStrength(int totalStrength) {
        this.totalStrength = totalStrength;
    }

    public int getTotalWill() {
        return totalWill;
    }

    public void setTotalWill(int totalWill) {
        this.totalWill = totalWill;
    }

    public int getTotalMagic() {
        return totalMagic;
    }

    public void setTotalMagic(int totalMagic) {
        this.totalMagic = totalMagic;
    }

    public Belief getBelief() {
        return belief;
    }

    public void setBelief(Belief belief) {
        this.belief = belief;
    }

    public List<Hero> getHeroes() {
        return heroes;
    }

    public void setHeroes(List<Hero> heroes) {
        this.heroes = heroes;
    }
}
