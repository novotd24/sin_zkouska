package com.example.zkouska.model;

import jakarta.persistence.*;

@Entity
@Table(name = "Hero")
public class Hero extends AbstractEntity {

    @Column(unique=true)
    private String name;
    private Belief belief;
    private int strength;
    private int will;
    private int magic;

    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

    public Hero() {
    }

    public Hero(String name, Belief belief, int strength, int will, int magic, Group group) {
        this.name = name;
        this.belief = belief;
        this.strength = strength;
        this.will = will;
        this.magic = magic;
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Belief getBelief() {
        return belief;
    }

    public void setBelief(Belief belief) {
        this.belief = belief;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getWill() {
        return will;
    }

    public void setWill(int will) {
        this.will = will;
    }

    public int getMagic() {
        return magic;
    }

    public void setMagic(int magic) {
        this.magic = magic;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
