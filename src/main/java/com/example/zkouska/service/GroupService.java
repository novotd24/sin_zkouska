package com.example.zkouska.service;

import com.example.zkouska.model.Group;

public interface GroupService {

    Group fight(Long groupId1, Long groupId2) throws Exception;
}
