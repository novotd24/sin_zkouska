package com.example.zkouska.service;

import com.example.zkouska.model.Belief;
import com.example.zkouska.model.Group;
import com.example.zkouska.model.Hero;

import java.util.List;

public interface HeroService {

    Hero addHeroToGroup(Long heroId, Long groupId);
    Hero getById(Long heroId);
    List<Hero> getAll();
    void saveHero(String name, Belief belief, int strength, int will, int magic);
    void updateHero(Long id, String name, Belief belief, int strength, int will, int magic);
}
