package com.example.zkouska.service;

import com.example.zkouska.model.Belief;
import com.example.zkouska.model.Group;
import com.example.zkouska.model.Hero;
import com.example.zkouska.repository.GroupRepository;
import com.example.zkouska.repository.HeroRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class HeroServiceImpl implements HeroService {

    private static final Logger logger = LoggerFactory.getLogger(HeroServiceImpl.class);

    private final HeroRepository heroRepository;
    private final GroupRepository groupRepository;

    @Autowired
    public HeroServiceImpl(HeroRepository heroRepository, GroupRepository groupRepository) {
        this.heroRepository = heroRepository;
        this.groupRepository = groupRepository;
    }

    /**
     * Adds hero to a group.
     * @param heroId id of the hero which should be added in the group
     * @param groupId id of the group
     * @return the added hero
     */
    @Cacheable("hero")
    @Override
    public Hero addHeroToGroup(Long heroId, Long groupId) {
        Optional<Hero> h = heroRepository.findById(heroId);
        Hero hero = new Hero();
        if (h.isPresent()) {
            hero = h.get();
        }
        Optional<Group> g = groupRepository.findById(groupId);
        Group group = new Group();
        if (g.isPresent()) {
            group = g.get();
        }
        if (hero.getBelief().equals(group.getBelief())) {
            group.getHeroes().add(hero);
            hero.setGroup(group);
            group.setTotalStrength(group.getTotalStrength() + hero.getStrength());
            group.setTotalWill(group.getTotalWill() + hero.getWill());
            group.setTotalMagic(group.getTotalMagic() + hero.getMagic());
        } else {
            logger.warn("Hero has different belief than the group! Hero ID: {}, Group ID: {}", heroId, groupId);
        }
        logger.info("Hero was added to the group");
        return hero;
    }

    @Cacheable("hero")
    @Override
    public Hero getById(Long heroId) {
        Optional<Hero> h = heroRepository.findById(heroId);
        return h.orElse(null);
    }

    @Override
    public List<Hero> getAll() {
        return (List<Hero>) heroRepository.findAll();
    }

    @Override
    public void saveHero(String name, Belief belief, int strength, int will, int magic) {
        Hero hero = new Hero(name, belief, strength, will, magic, null);
        heroRepository.save(hero);
    }

    /**
     * Updates hero.
     * @param heroId id of the hero which should be changed
     */
    @Override
    public void updateHero(Long heroId, String name, Belief belief, int strength, int will, int magic) {
        Optional<Hero> h = heroRepository.findById(heroId);
        Hero hero = new Hero();
        if (h.isPresent()) {
            hero = h.get();
        }
        hero.setName(name);
        if (hero.getGroup() != null && hero.getGroup().getBelief().equals(belief)) {
            hero.setBelief(belief);
        }
        hero.setStrength(strength);
        hero.setWill(will);
        hero.setMagic(magic);
        heroRepository.save(hero);
    }
}
