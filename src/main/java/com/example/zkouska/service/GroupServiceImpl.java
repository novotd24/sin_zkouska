package com.example.zkouska.service;

import com.example.zkouska.model.Group;
import com.example.zkouska.repository.GroupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

@Service
public class GroupServiceImpl implements GroupService {

    private static final Logger logger = LoggerFactory.getLogger(HeroServiceImpl.class);

    private final GroupRepository groupRepository;

    @Autowired
    public GroupServiceImpl(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    /**
     * Performs a fight between two groups with different belief.
     * @param groupId1 id of the first group included in a fight
     * @param groupId2 id of the second group included in a fight
     * @return the group which wins the fight
     * @throws Exception fight cannot be executed
     */
    @Override
    public Group fight(Long groupId1, Long groupId2) throws Exception {
        Optional<Group> g1 = groupRepository.findById(groupId1);
        Optional<Group> g2 = groupRepository.findById(groupId2);
        Group group1, group2;
        if (g1.isPresent() && g2.isPresent()) {
            group1 = g1.get();
            group2 = g2.get();
            if (!group1.getBelief().equals(group2.getBelief())) {
                return getWinnerGroup(group1, group2);
            } else {
                logger.error("Groups have the same belief!");
                throw new Exception("Groups have the same belief!");
            }
        } else {
            logger.error("Groups doesn't exist!");
            throw new Exception("Groups doesn't exist!");
        }
    }

    public Group getWinnerGroup(Group group1, Group group2) throws Exception {
        Random random = new Random();
        int fightSpecification = random.nextInt(3); //1 for strength, 2 for will, 3 for magic
        int totalDamage1 = 0;
        int totalDamage2 = 0;
        switch (fightSpecification) {
            case 0 -> {
                totalDamage1 = group1.getTotalStrength();
                totalDamage2 = group2.getTotalStrength();
            }
            case 1 -> {
                totalDamage1 = group1.getTotalWill();
                totalDamage2 = group2.getTotalWill();
            }
            case 2 -> {
                totalDamage1 = group1.getTotalMagic();
                totalDamage2 = group2.getTotalMagic();
            }
            default -> {
            }
        }
        if (totalDamage1 == totalDamage2) {
            return fight(group1.getId(), group2.getId());
        } else if (totalDamage1 > totalDamage2) {
            logger.info("Group one won the fight.");
            return group1;
        } else {
            logger.info("Group two won the fight.");
            return group2;
        }
    }
}
