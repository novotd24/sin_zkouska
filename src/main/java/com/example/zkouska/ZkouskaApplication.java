package com.example.zkouska;

import com.example.zkouska.model.Belief;
import com.example.zkouska.model.Group;
import com.example.zkouska.model.Hero;
import com.example.zkouska.repository.GroupRepository;
import com.example.zkouska.repository.HeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ZkouskaApplication implements CommandLineRunner {

    @Autowired
    private HeroRepository heroRepository;
    @Autowired
    private GroupRepository groupRepository;

    public static void main(String[] args) {
        SpringApplication.run(ZkouskaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Hero gandalf = new Hero("gandalf", Belief.GOOD, 2,5,10, null);
        Hero bilbo = new Hero("bilbo", Belief.GOOD, 3,10,1, null);
        Hero thorin = new Hero("thorin", Belief.GOOD, 10,5,0, null);
        Hero bolg = new Hero("bolg", Belief.EVIL, 10,10,0, null);
        Hero golfimbul = new Hero("golfimbul", Belief.EVIL, 6,4,0, null);
        Hero necromancer = new Hero("necromancer", Belief.EVIL, 3,3,10, null);
        Group goodGroup = new Group(0,0,0, Belief.GOOD, new ArrayList<>());
        Group evilGroup = new Group(0,0,0, Belief.EVIL, new ArrayList<>());
//        Group goodGroup = new Group(14,27,5, Belief.GOOD, new ArrayList<>());
//        Group evilGroup = new Group(10,15,21, Belief.EVIL, new ArrayList<>());

        heroRepository.save(gandalf);
        heroRepository.save(bilbo);
        heroRepository.save(thorin);
        heroRepository.save(bolg);
        heroRepository.save(golfimbul);
        heroRepository.save(necromancer);
        groupRepository.save(goodGroup);
        groupRepository.save(evilGroup);

        System.out.println("GROUP IDS");
        System.out.println(goodGroup.getId());
        System.out.println(evilGroup.getId());


    }
}
