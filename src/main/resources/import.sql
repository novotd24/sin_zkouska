CREATE TABLE Hero (
                      id BIGINT NOT NULL AUTO_INCREMENT,
                      name VARCHAR(255) UNIQUE,
                      belief VARCHAR(255),
                      strength INT,
                      will INT,
                      magic INT,
                      group_id BIGINT,
                      PRIMARY KEY (id),
                      FOREIGN KEY (group_id) REFERENCES Group(id)
);

CREATE TABLE Group (
                       id BIGINT NOT NULL AUTO_INCREMENT,
                       totalStrength INT,
                       totalWill INT,
                       totalMagic INT,
                       belief VARCHAR(255),
                       PRIMARY KEY (id)
);

INSERT INTO Group (totalStrength, totalWill, totalMagic, belief) VALUES
                                                                     (0, 0, 0, 'GOOD'),
                                                                     (0, 0, 0, 'EVIL');

INSERT INTO Hero (name, belief, strength, will, magic, group_id) VALUES
                                                                     ('gandalf', 'GOOD', 2,5,10,1),
                                                                     ('bilbo', 'GOOD', 3,10,1,1),
                                                                     ('thorin', 'GOOD', 10,5,0,1),
                                                                     ('bolg', 'EVIL', 10,10,0,2),
                                                                     ('golfimbul', 'EVIL', 6,4,0,2),
                                                                     ('necromancer', 'EVIL', 3,3,10,2);
